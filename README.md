
# Multi Modal Human Health Scanner

## Overview
The Multi Modal Human Health Scanner leverages state-of-the-art AI technologies to assess the health status of individuals by analyzing images captured through a webcam. The system utilizes CLIP (Contrastive Language-Image Pretraining) for image description and an advanced LLM (Large Language Model) for health status analysis, ensuring accurate and reliable results.

## Features
- Capture images using a webcam.
- Describe the captured image using CLIP.
- Analyze health status using an advanced LLM.
- Store image embeddings in a SQLite database.
- Log detailed execution steps for better traceability.

## Requirements
- Python 3.7 or higher
- aiohttp
- aiosqlite
- transformers
- Pillow
- opencv-python
- torch
- torchvision
- numpy
- llama-cpp-python
- logging

## Installation

1. **Clone the Repository**
    ```sh
    git clone https://github.com/yourusername/multi-modal-human-health-scanner.git
    cd multi-modal-human-health-scanner
    ```

2. **Install Dependencies**
    ```sh
    pip install -r requirements.txt
    ```

## Configuration

Ensure the `DATABASE_FILE` and `LOCAL_IMAGE_DIR` are set correctly in the script:

```python
DATABASE_FILE = 'data.db'
LOCAL_IMAGE_DIR = 'images/'
```

## Usage

1. **Run the Script**
    ```sh
    python main.py
    ```

2. **Follow the Prompts**
    - The script will initialize the database and prepare the environment.
    - Ensure your webcam is connected and accessible.
    - The system will capture an image, describe it, and analyze the health status.
    - Results will be displayed in the console.

## How the Script Works

### 1. Initialization
The script starts by initializing the SQLite database to store image embeddings:

```python
async def init_database():
    async with aiosqlite.connect(DATABASE_FILE) as conn:
        await conn.execute('CREATE TABLE IF NOT EXISTS image_embeddings (id INTEGER PRIMARY KEY AUTOINCREMENT, embedding TEXT, image_path TEXT)')
        await conn.commit()
```

### 2. Capture Image
An image is captured using the webcam:

```python
async def capture_image():
    cap = cv2.VideoCapture(0)
    if not cap.isOpened():
        logging.error("Unable to access the webcam.")
        return None
    ret, frame = cap.read()
    cap.release()
    if ret:
        return frame
    else:
        logging.error("Unable to capture an image from the webcam.")
        return None
```

### 3. Describe Image
The image is described using the CLIP model:

```python
async def describe_image(image, processor, model):
    inputs = processor(images=[image], return_tensors="pt", padding=True)
    outputs = model(**inputs)
    logits_per_image = outputs.logits_per_image
    probs = logits_per_image.softmax(dim=1)
    description = "This image likely contains: " + ', '.join([f"{p:.2%} {label}" for label, p in zip(inputs['input_text'], probs.squeeze().tolist())])
    return description
```

### 4. Store Embedding
The image embedding is stored in the database:

```python
async def store_image_embedding(embedding, image_path):
    async with aiosqlite.connect(DATABASE_FILE) as conn:
        await conn.execute('INSERT INTO image_embeddings (embedding, image_path) VALUES (?, ?)', (str(embedding), image_path))
        await conn.commit()
```

### 5. Save Image Locally
The captured image is saved locally:

```python
def save_image_locally(image):
    image_file_name = ''.join(random.choices(string.ascii_letters + string.digits, k=10)) + '.jpg'
    image_file_path = os.path.join(LOCAL_IMAGE_DIR, image_file_name)
    cv2.imwrite(image_file_path, image)
    return image_file_path
```

### 6. Process with Health Monitoring
The image is processed with the health monitoring system:

```python
async def process_image_with_human_health_monitoring(image, image_description, chat_handler, llm):
    image_b64 = base64.b64encode(cv2.imencode('.jpg', image)[1]).decode("utf-8")
    response = llm.create_chat_completion(
        messages=[
            {
                "role": "user",
                "content": [
                    {"type": "text", "text": f"Scan for human health status. Description: {image_description}"},
                    {"type": "image_base64", "image_base64": image_b64}
                ]
            }
        ]
    )
    return response["choices"][0]["text"]
```

### 7. Main Function
The main function ties everything together:

```python
async def main():
    await init_database()
    if not os.path.exists(LOCAL_IMAGE_DIR):
        os.makedirs(LOCAL_IMAGE_DIR)

    processor = CLIPProcessor.from_pretrained(CLIP_MODEL_PATH)
    model = CLIPModel.from_pretrained(CLIP_MODEL_PATH)
    chat_handler = MoondreamChatHandler.from_pretrained(repo_id=MOONDREAM_MODEL_REPO_ID, filename=MOONDREAM_CHAT_HANDLER_FILENAME)
    llm = Llama.from_pretrained(repo_id=MOONDREAM_MODEL_REPO_ID, filename=MOONDREAM_MODEL_FILENAME, chat_handler=chat_handler, n_ctx=2048)

    image = await capture_image()
    if image is not None:
        image_description = await describe_image(image, processor, model)
        if image_description:
            embeddings = image_description.last_hidden_state.mean(dim=1).squeeze().detach().numpy()
            image_path = save_image_locally(image)
            await store_image_embedding(embeddings, image_path)
            health_monitoring_response = await process_image_with_human_health_monitoring(image, image_description, chat_handler, llm)
            if health_monitoring_response:
                print("Image Description:", image_description)
                print("Human Health Monitoring Response:", health_monitoring_response)
            else:
                logging.error("Human Health Monitoring response is empty.")
        else:
            logging.error("Image description is empty.")
    else:
        logging.error("No image captured.")

if __name__ == "__main__":
    print(HUMAN_HEALTH_PROMPT)
    asyncio.run(main())
```
This project integrates multiple AI technologies to create a robust system for human health monitoring. The detailed logging and error handling ensure that the system is reliable and easy to debug. Follow the instructions above to install, configure, and run the Multi Modal Human Health Scanner.
